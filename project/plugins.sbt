/* Plugin for scalaformat */
addSbtPlugin("org.scalameta" % "sbt-scalafmt" % "2.0.5")
/* Plugin for coverage test */
addSbtPlugin("org.scoverage" % "sbt-scoverage" % "1.6.0")
