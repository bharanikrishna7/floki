package com.floki

import com.floki.auth.BasicAuthParams
import wvlet.log.{LogFormatter, Logger, LogSupport}

import scala.collection.mutable

object network extends LogSupport {
  Logger.setDefaultFormatter(LogFormatter.SourceCodeLogFormatter)

  /**
    * Case class to hold response received from GET/POST Requests.
    * @param status HTTP Status code associated with response.
    * @param headers Headers associated with response.
    * @param body Body of the response.
    * @tparam T Case Class/Class associated with the body.
    */
  case class Response[T](
      status: Int,
      headers: Map[String, List[String]],
      body: T
  )

  /**
    * Enumerable Object to hold various supported HTTP
    * requests by Floki.
    */
  object HttpMethod {
    sealed trait EnumVal
    case object GET extends EnumVal
    case object POST extends EnumVal
    case object DELETE extends EnumVal
    val methods = Seq(GET, POST, DELETE)
  }

  /**
    * Method to help generate URL when path and query
    * string arguments are supplied.
    * @param path
    * @param queryStringParams
    * @return
    */
  def generateURI(
      path: String,
      queryStringParams: Map[String, String]
  ): String = {
    var queryString = ""
    var first: Boolean = true
    for ((k, v) <- queryStringParams) {
      if (first) {
        queryString = s"?"
        first = false
      } else {
        queryString = s"&"
      }
      queryString = s"${queryString}${k}=${v}"
    }
    s"${path}${queryString}"
  }

  /**
    * Method to make a get request.
    * @param url url to hit.
    * @param connectionTimeout max connection alive timeout.
    * @param readTimeout maximum timeout to read the response from request.
    * @param authParams optional basic authentication parameters.
    * @return Response with String Type Body.
    */
  def get(
      url: String,
      connectionTimeout: Int = 5000,
      readTimeout: Int = 5000,
      authParams: Option[BasicAuthParams] = None
  ): Response[String] = {
    request(
      url,
      connectionTimeout,
      readTimeout,
      HttpMethod.GET,
      None,
      authParams
    )
  }

  /**
    * Method to make a get request.
    * @param url url to hit.
    * @param connectionTimeout max connection alive timeout.
    * @param readTimeout maximum timeout to read the response from request.
    * @param m Manifest associated with case class / class, which we want to parse to. [Implicitly taken care of]
    * @tparam T case class / class we are expecting the response body in.
    * @param authParams optional basic authentication parameters.
    * @return Response with Supplied 'T' Type Body.
    */
  def getPOJO[T](
      url: String,
      connectionTimeout: Int = 5000,
      readTimeout: Int = 5000,
      authParams: Option[BasicAuthParams] = None
  )(implicit m: Manifest[T]): Response[T] = {
    val result: Response[String] =
      request(
        url,
        connectionTimeout,
        readTimeout,
        HttpMethod.GET,
        None,
        authParams
      )
    val body: T = json.parse[T](result.body)
    Response[T](result.status, result.headers, body)
  }

  /**
    * Function to make HTTP Requests.
    * @param url request URL
    * @param connectTimeout connection timeout
    * @param readTimeout read timeout
    * @param method Http Method
    * @param authParams optional basic authentication parameters.
    * @return Returns response with string body.
    */
  def request(
      url: String,
      connectTimeout: Int = 5000,
      readTimeout: Int = 5000,
      method: HttpMethod.EnumVal = HttpMethod.GET,
      requestBody: Option[String] = None,
      authParams: Option[BasicAuthParams] = None
  ): Response[String] = {
    import java.net.{URL, HttpURLConnection}
    import scala.collection.JavaConverters._

    val connection: HttpURLConnection =
      (new URL(url)).openConnection.asInstanceOf[HttpURLConnection]
    connection.setConnectTimeout(connectTimeout)
    connection.setReadTimeout(readTimeout)
    connection.setRequestMethod(method.toString)
    if (authParams.isDefined) {
      connection.setRequestProperty(
        "Authorization",
        auth.generateBasicAuthToken(authParams.get)
      )
    }
    if (method == HttpMethod.POST && requestBody.isDefined) {
      logger.debug(s"Making necessary modifications for POST Request.")
      connection.setDoOutput(true)
      connection.getOutputStream.write(requestBody.get.getBytes("utf-8"))
    }
    val inputStream = connection.getInputStream
    val body: String = scala.io.Source.fromInputStream(inputStream).mkString
    val headerRaw: java.util.Map[String, java.util.List[String]] =
      connection.getHeaderFields

    var headers: mutable.Map[String, List[String]] =
      mutable.Map[String, List[String]]()
    for (key: String <- headerRaw.keySet().asScala.toSet) {
      val value: List[String] = headerRaw.get(key).asScala.toList
      headers += (key -> value)
    }
    val status: Int = connection.getResponseCode
    Response[String](status, headers.toMap, body)
  }

  /**
    * Method to make post requests
    * @param url url to hit.
    * @param body Post request body.
    * @param connectionTimeout max connection alive timeout.
    * @param readTimeout maximum timeout to read the response from request.
    * @param authParams optional basic authentication parameters.
    * @return Response with String Type Body.
    */
  def post(
      url: String,
      body: Option[String],
      connectionTimeout: Int = 5000,
      readTimeout: Int = 5000,
      authParams: Option[BasicAuthParams] = None
  ): Response[String] = {
    request(
      url,
      connectionTimeout,
      readTimeout,
      HttpMethod.POST,
      body,
      authParams
    )
  }

  /**
    * Method to make post request with supplied POJO Body.
    * @param url url to hit.
    * @param body Post request body.
    * @param connectionTimeout max connection alive timeout.
    * @param readTimeout maximum timeout to read the response from request.
    * @param m Manifest associated with case class / class, of the body. [Implicitly taken care of]
    * @tparam T case class / class associated with the body.
    * @param authParams optional basic authentication parameters.
    * @return Response with body as string.
    */
  def postPOJO[T](
      url: String,
      body: T,
      connectionTimeout: Int = 5000,
      readTimeout: Int = 5000,
      authParams: Option[BasicAuthParams] = None
  )(implicit m: Manifest[T]): Response[String] = {
    val processedBody: Option[String] = Some(json.toString(body))
    post(url, processedBody, connectionTimeout, readTimeout, authParams)
  }

  /**
    * Method to make delete requests.
    * @param url url to hit.
    * @param connectionTimeout max connection alive timeout.
    * @param readTimeout maximum timeout to read the response from request.
    * @param authParams optional basic authentication parameters.
    * @return Response with body as string.
    */
  def delete(
      url: String,
      connectionTimeout: Int = 5000,
      readTimeout: Int = 5000,
      authParams: Option[BasicAuthParams] = None
  ): Response[String] = {
    request(
      url,
      connectionTimeout,
      readTimeout,
      HttpMethod.DELETE,
      None,
      authParams
    )
  }
}
