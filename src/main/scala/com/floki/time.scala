package com.floki

import java.lang.System.nanoTime
import java.text.SimpleDateFormat
import java.time.Instant
import java.util.{Calendar, Date, TimeZone}

import wvlet.log.LogSupport

object time extends LogSupport {

  /**
    * Enumeration object to hold various
    * time units.
    */
  object TimeUnits extends Enumeration {
    type TimeUnits = Value

    val nanoseconds = Value(1)
    val microseconds = Value(2)
    val milliseconds = Value(3)
    val seconds = Value(4)
    val minutes = Value(5)
    val hours = Value(6)

    /**
      * Method to retrieve the smallest.
      * available time unit
      * @return Smallest time unit available to user
      */
    def first: TimeUnits.Value = {
      nanoseconds
    }

    /**
      * Method to retrieve the largest.
      * available time unit
      * @return Largest time unit available to user
      */
    def last: TimeUnits.Value = {
      hours
    }
  }

  /**
    * Function to get next time unit in the enumeration.
    * @param unit Current time unit
    * @return Next time unit (with respect to supplied time unit)
    */
  def getNextUnit(unit: TimeUnits.Value): TimeUnits.Value = {
    val allUnits = TimeUnits.values
    var foundCurrentUnit: Boolean = false
    var result: TimeUnits.Value = TimeUnits.hours
    for (value <- allUnits) {
      if (foundCurrentUnit) {
        result = value
        foundCurrentUnit = false
      }
      if (value == unit) {
        foundCurrentUnit = true
      }
    }
    result
  }

  /**
    * Function to get previous time unit in the enumeration.
    * @param unit Current time unit
    * @return Previous time unit (with respect to supplied time unit)
    */
  def getPreviousUnit(unit: TimeUnits.Value): TimeUnits.Value = {
    val allUnits = TimeUnits.values
    var result: TimeUnits.Value = TimeUnits.nanoseconds
    for (value <- allUnits) {
      if (value == unit) {
        return result
      }
      result = value
    }
    result
  }

  /**
    * Method to get current timestamp.
    * @param tz Timezone for the current timestamp [Defaults to JVM Timezone]
    * @return Current timestamp as string
    */
  def getCurrentTimestamp(
      tz: TimeZone = Calendar.getInstance().getTimeZone
  ): String = {
    val date: Date = new Date()
    val format: SimpleDateFormat = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss")
    format.setTimeZone(tz)
    format.format(date)
  }

  /**
    * Method to time how long does a block of code take to execute.
    *
    * Can be used for benchmark purposes.
    * @param block Function / Code Block of code to benchmark
    * @tparam R Response Type of the function / code block
    * @return Returns the result of function / coded block execution
    */
  def stopclock[R](block: => R): R = {
    val startTime: Double = nanoTime
    val result = block
    val stopTime: Double = nanoTime
    val difference: Double = stopTime - startTime
    info(
      s"Time taken to execute this code block : ${nanoTimeToString(difference)}"
    )
    result
  }

  /**
    * Method to convert nanotime to string value.
    * @param value Time in nanoseconds.
    * @param unit Conversion unit.
    * @return Time converted such that value is between [0 - 1000]
    */
  def nanoTimeToString(
      value: Double,
      unit: TimeUnits.Value = TimeUnits.first
  ): String = {
    var result: String = ""
    if (value > 1000d) {
      result = nanoTimeToString(value / 1000d, getNextUnit(unit))
    } else {
      result = s"""${value} ${unit}"""
    }
    result
  }
}
