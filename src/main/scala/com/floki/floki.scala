package com.floki

import wvlet.log._

object floki extends App with LogSupport {
  Logger.setDefaultFormatter(LogFormatter.SourceCodeLogFormatter)
  info(s"Floki is a library. Executing JAR will do nothing.")
}
