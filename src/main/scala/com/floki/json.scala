package com.floki

import org.json4s.JsonAST.JValue
import org.json4s._
import org.json4s.native.JsonMethods
import wvlet.log.{LogFormatter, LogSupport, Logger}

object json extends LogSupport {
  Logger.setDefaultFormatter(LogFormatter.SourceCodeLogFormatter)
  implicit val formats = DefaultFormats

  /**
    * Method to parse a string (JSON) to Java
    * or Scala Class.
    * @param json
    * @tparam T
    * @return
    */
  def parse[T](json: String)(implicit m: Manifest[T]): T = {
    try {
      val parsed: JValue = JsonMethods.parse(json)
      return parsed.extract[T]
    } catch {
      case e: Exception => {
        error(s"Actual Exception Message : ${e.getMessage}")
        error(s"Encountered error while extracting Object from JSON String.")
        error(
          s"Please ensure that this method is call as shown in below example:"
        )
        error(s"ex: ")
        error(s"val json: String")
        error(
          s"val payload : T = json.parse[T](json)   // where T can be class."
        )
        throw new Exception("Error Code : 501")
      }
    }
  }

  /**
    * Method to check if a given string is JSON parsable.
    * @param payload
    * @return
    */
  def isJson(payload: String): Boolean = {
    var result: Boolean = false
    try {
      val parsed: JValue = JsonMethods.parse(payload)
      result = true
    } catch {
      case e: Exception => {
        error(s"Encountered error while parsing JSON to JValue.")
        error(s"Actual Exception Message : ${e.getMessage}")
      }
    }
    result
  }

  /**
    * Method to convert Object into String.
    * @param payload
    * @tparam T
    * @return
    */
  def toString[T](payload: T)(implicit m: Manifest[T]): String = {
    try {
      val parsed: String =
        JsonMethods.compact(JsonMethods.render(toJValue[T](payload)))
      parsed
    } catch {
      case e: Exception => {
        error(s"Actual Exception Message : ${e.getMessage}")
        error(s"Encountered error while converting Object to String.")
        error(
          s"Please ensure that this method is call as shown in below example:"
        )
        error(s"ex: ")
        error(s"val T: payload                         // where T is a class")
        error(s"val stringValueOfObject : String = json.toString[T](payload)")
        throw new Exception("Error Code : 502")
      }
    }
  }

  /**
    * Method to convert Object to JValue.
    * @param payload
    * @tparam T
    * @return
    */
  def toJValue[T](payload: T)(implicit m: Manifest[T]): JValue = {
    try {
      Extraction.decompose(payload)
    } catch {
      case e: Exception => {
        error(s"Actual Exception Message : ${e.getMessage}")
        error(s"Encountered error while converting Object to JValue.")
        error(
          s"Please ensure that this method is call as shown in below example:"
        )
        error(s"ex: ")
        error(s"val T: payload            // where T can be class or string.")
        error(s"val stringValueOfObject : JValue = json.toJValue[T](payload)")
        throw new Exception("Error Code : 503")
      }
    }
  }

  /**
    * Method to compare 2 json strings and return true if they contain same content/values.
    * @param json0 first json string
    * @param json1 second json string
    * @return true if value of json0 == value of json1 else false.
    */
  def compare(json0: String, json1: String): Boolean = {
    val jvalue0: JValue = toJValue(json0)
    val jvalue1: JValue = toJValue(json1)
    val difference: Diff = jvalue0 diff jvalue1
    var result: Boolean = false
    if (difference.deleted == JNothing && difference.added == JNothing && difference.changed == JNothing) {
      result = true
    }
    result
  }
}
