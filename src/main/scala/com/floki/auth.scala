package com.floki

import wvlet.log.{LogFormatter, Logger, LogSupport}

object auth extends LogSupport {
  Logger.setDefaultFormatter(LogFormatter.SourceCodeLogFormatter)

  case class BasicAuthParams(username: String, password: String)

  /**
    * Function to generate basic auth token
    * for basic authentication.
    * @param username associated username
    * @param password associated password
    * @return basic authentication token as string
    */
  def generateBasicAuthToken(username: String, password: String): String = {
    import java.util.Base64
    val userPass: String = s"$username:$password"
    val token = s"Basic : ${Base64.getEncoder.encode(userPass.getBytes)}"
    token
  }

  /**
    * Function to generate basic auth token
    * for basic authentication.
    * @param basicAuthParams associated parameters
    * @return basic authentication token as string
    */
  def generateBasicAuthToken(basicAuthParams: BasicAuthParams): String = {
    generateBasicAuthToken(basicAuthParams.username, basicAuthParams.password)
  }
}
