package com.floki

import com.danielasfregola.randomdatagenerator.RandomDataGenerator
import org.scalatest.FlatSpec
import wvlet.log.LogSupport

class jsonSpec extends FlatSpec with RandomDataGenerator with LogSupport {
  case class ExampleChildJson(integer: Int, long: Long, string: String, bigInt: BigInt, bigDecimal: BigDecimal)
  case class ExampleParentJson(list: List[ExampleChildJson], map: Map[Int, ExampleChildJson])

  "toString method" should "properly convert the object to String" in {
    val payload: ExampleChildJson = ExampleChildJson(1, 2L, "String Value", BigInt("100000000000000"), BigDecimal("2.5028137654082454757164012349"))
    val actual: String = json.toString[ExampleChildJson](payload)
    val expected: String = "{\"integer\":1,\"long\":2,\"string\":\"String Value\",\"bigInt\":100000000000000,\"bigDecimal\":2.5028137654082454}"
    info(s"Actual : ${actual}")
    info(s"Expected : ${expected}")
    assert(actual == expected)
  }

  "toString method" should "properly convert complex objects to String" in {
    val payload: ExampleParentJson = random[ExampleParentJson]
    val actual: String = json.toString[ExampleParentJson](payload)
    info(s"Actual Value : ${actual}")
    val processed: ExampleParentJson = json.parse[ExampleParentJson](actual)
    val expected : String = json.toString[ExampleParentJson](processed)
    info(s"Expected Value : ${expected}")
    assert(json.compare(actual, expected))
  }

  "compare method" should "properly compare 2 json strings and return false when both represent different JSON Object" in {
    val payloads: Seq[ExampleParentJson] = random[ExampleParentJson](2)
    val json0: String = json.toString[ExampleParentJson](payloads(0))

    val json1: String = json.toString[ExampleParentJson](payloads(1))
    info(s"JSON 0 : ${json0}")
    info(s"JSON 1 : ${json1}")
    assert(json.compare(json0, json1) == false)
  }

  "toString method" should "throw error when invalid JSON is supplied" in {
    val payload: String = "{\"integer\":1,\"long\":2,string:\"String Value\",\"bigInt\":1234,\"bigDecimal\":2.5028137654082454}"
    info(s"Supplied Corrupted JSON String : ${payload}")
    assertThrows[Exception] {
      val resultObject: ExampleChildJson = json.parse[ExampleChildJson](payload)
    }
  }

  "parse method" should "properly parse simple/complex JSON Objects from string" in {
    val simplePayload : ExampleChildJson  = random[ExampleChildJson]
    val complexPayload: ExampleParentJson = random[ExampleParentJson]
    val simpleStringPayload: String = json.toString[ExampleChildJson](simplePayload)
    val complexStringPayload: String = json.toString[ExampleParentJson](complexPayload)
    val simpleObject: ExampleChildJson = json.parse[ExampleChildJson](simpleStringPayload)
    val complexObject: ExampleParentJson = json.parse[ExampleParentJson](complexStringPayload)
    assert(true)
  }
}
