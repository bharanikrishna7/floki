package com.floki

import com.danielasfregola.randomdatagenerator.RandomDataGenerator
import com.floki.network.Response
import org.scalatest.FlatSpec
import wvlet.log.LogSupport

class networkSpec extends FlatSpec with RandomDataGenerator with LogSupport {
  val allEmployeesURL: String =
    "http://dummy.restapiexample.com/api/v1/employees"

  case class CreateEmployee(
      name: String,
      salary: String,
      age: String
  )

  case class PostRetrieveEmployee(
      id: String,
      name: String,
      salary: String,
      age: String
  )

  case class GetRetrieveEmployee(
      id: String,
      employee_name: String,
      employee_salary: String,
      employee_age: String,
      profile_image: String
  )

  "get method" should "properly retrieve the Response" in {
    val response: network.Response[String] = network.get(allEmployeesURL)
    info(s"Response Status Code : ${response.status}")
    info(s"Response Headers : ${response.headers.toString}")
    info(s"Response Body : ${response.body}")

    assert(response.status / 10 == 20)
  }

  "generateURI method" should "properly generate URL when path and 1 query string param are provided" in {
    val path: String = "http://google.com"
    val params: Map[String, String] = Map("key1" -> "value1")
    val expected: String = "http://google.com?key1=value1"
    val actual: String = network.generateURI(path, params)
    assert(expected == actual)
  }

  "Post and GET POJO Methods" should "behave as expected" in {
    logger.info(s"Create new object.")
    val postPath: String = "http://dummy.restapiexample.com/api/v1/create"
    val payload: CreateEmployee =
      CreateEmployee(name = "floki-test", salary = "75", age = "27")
    logger.info(
      s"Creating Object with Structure : ${json.toString[CreateEmployee](payload)}"
    )
    val createEntry: Response[String] =
      network.postPOJO[CreateEmployee](postPath, payload)
    val parsedResponse: PostRetrieveEmployee =
      json.parse[PostRetrieveEmployee](createEntry.body)
    logger.info(s"Retrieved POST Request Response : ${createEntry.body}")
    assert(payload.name == parsedResponse.name)
    assert(payload.age == parsedResponse.age)
    assert(payload.salary == parsedResponse.salary)
    logger.info(s"Retrieve created object.")
    val getPath: String =
      s"http://dummy.restapiexample.com/api/v1/employee/${parsedResponse.id}"
    val getResponse: Response[GetRetrieveEmployee] =
      network.getPOJO[GetRetrieveEmployee](getPath)
    val getBody: GetRetrieveEmployee = getResponse.body
    logger.info(
      s"Retrieved GET Response : ${json.toString[GetRetrieveEmployee](getBody)}"
    )
    logger.info(
      s"Comparing retrieved output with Response from POST to validate object."
    )
    assert(getBody.id == parsedResponse.id)
    assert(getBody.employee_name == parsedResponse.name)
    assert(getBody.employee_age == parsedResponse.age)
    assert(getBody.employee_salary == parsedResponse.salary)
    logger.info(s"Deleting created test object.")
    val delUrl: String =
      s"http://dummy.restapiexample.com/api/v1/delete/${parsedResponse.id}"
    val deleteResponse: Response[String] = network.delete(delUrl)
    logger.info(s"Retrieved Delete Response : ${deleteResponse.body}")
    assert(true)
  }
}
