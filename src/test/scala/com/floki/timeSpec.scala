package com.floki

import java.util.TimeZone

import com.danielasfregola.randomdatagenerator.RandomDataGenerator
import com.floki.time.TimeUnits
import org.scalatest.FlatSpec
import wvlet.log.LogSupport

class timeSpec extends FlatSpec with RandomDataGenerator with LogSupport {
  "getNextUnit Method" should "correctly fetch the next unit." in {
    var iterator = TimeUnits.values.iterator
    var slowUnit: TimeUnits.Value = iterator.next()
    while (iterator.hasNext) {
      val expected: TimeUnits.Value = time.getNextUnit(slowUnit)
      val actual: TimeUnits.Value = iterator.next()
      slowUnit = expected
      info(s"Actual Value : ${actual}")
      info(s"Expected Value : ${expected}")
      assert(expected == actual)
    }
  }

  "getPreviousUnit Method" should "correctly fetch the previous unit." in {
    val units: List[TimeUnits.Value] = TimeUnits.values.toList
    val maxIndex: Int = units.size - 1
    var current: TimeUnits.Value = units(maxIndex)
    for(i <- maxIndex -1 to 0) {
      val expected: TimeUnits.Value = time.getPreviousUnit(current)
      val actual: TimeUnits.Value = units(i)
      current = actual
      info(s"Actual Value : ${actual}")
      info(s"Expected Value : ${expected}")
      assert(expected == actual)
    }
  }

  case class ExampleClass(id: Int, firstName: String, lastName: String, email: String)

  private def task(load: Int = 100): Int = {
    var result : Int = 0
    for(i <- 1 to load) {
      val payload: ExampleClass = random[ExampleClass]
      result = result + 1
    }
    result
  }

  "stopclock Method" should "correctly execute the function and return result while displaying log to console." in {
    val load: Int = 10
    val actual: Int = time.stopclock(task(load))
    info(s"Expected Value : ${load}")
    info(s"Actual Value : ${actual}")
    assert(load == actual)
  }

  "getCurrentTimestamp Method" should "retrieve timestamp correctly when called (without timezone)." in {
    val timestamp: String = time.getCurrentTimestamp()
    info(s"Timestamp retrieved from method : ${timestamp}")
    assert(true)
  }

  "getCurrentTimestamp Method" should "retrieve timestamp correctly when called (with timezone)." in {
    val tz: TimeZone = TimeZone.getTimeZone("UTC")
    val timestamp: String = time.getCurrentTimestamp(tz)
    info(s"UTC Timestamp retrieved from method : ${timestamp}")
    assert(true)
  }
}
