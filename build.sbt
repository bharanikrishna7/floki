name := "floki"
version := "0.5.0"
scalaVersion := "2.13.0"
organization := "com.chekuri"
developers := List(
  Developer(
    "bharanikrishna7",
    "Venkata Bharani Krishna Chekuri",
    "bharanikrishna7@gmail.com",
    null
  )
)

// Library for logging
libraryDependencies += "org.wvlet.airframe" %% "airframe-log" % "19.7.3"
// Library to parse Json to objects and vice-versa
libraryDependencies += "org.json4s" %% "json4s-native" % "3.6.7"
// Library to generate random data for unit tests
libraryDependencies += "com.danielasfregola" %% "random-data-generator" % "2.7"
// Libraries for unit tests
libraryDependencies += "org.scalactic" %% "scalactic" % "3.0.8"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.8" % "test"
