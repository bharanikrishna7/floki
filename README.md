# floki

sbt / maven library which contains common utilities that are nicely documented and easy to use while not taking any noticeable hit to performance.

CircleCI Status : [![CircleCI](https://circleci.com/gh/bharanikrishna7/floki/tree/master.svg?style=svg)](https://circleci.com/gh/bharanikrishna7/floki/tree/master)

Coverage Report: [![codecov](https://codecov.io/gh/bharanikrishna7/floki/branch/master/graph/badge.svg)](https://codecov.io/gh/bharanikrishna7/floki)


## Feature Progress
* [x]  json
    * [x] parse
    * [x] toString
    * [x] compare
* [x] time
    * [x] stopclock
    * [x] getCurrentTimestamp
        * [x] without timezone
        * [x] with timezone
* [x] network
    * [x] get
        * [x] without auth
        * [x] with auth
        * [x] with result parsed to POJO/case class
        * [x] with query string params and results parsed to POJO/case class 
    * [x] post
        * [x] without auth
        * [x] with auth
        * [x] with POJO/case class in body
    * [x] delete
        * [x] without auth
        * [x] with auth

## Implemented Features
The features offered by Floki will be listed below:
### JSON
json object contains methods which make parsing string to POGO/Case class or vice-versa easier. It has been designed such as to minimize the efforts put by developer while handling Json objects while not taking a considerable hit to performance.

`parse : parse method handles parsing of a json string to POJO/Case class`
```scala
        case class Numbers(integer: Int, long: Long)
        val json : String = "{\"integer\":1,\"long\":2}"
        val parsed: Numbers = json.parse[Numbers](json)
```
`toString : toString method handles converting a POJO/case class to json string`
```scala
    case class Person(id: Long, firstName: String, lastName: String, email: String)
    val payload: Person = Person(7, "Venkata", "Chekuri", "bharanikrishna7@gmail.com")
    val json: String = json.toString[Person](payload)
```
`compare : compare method compares 2 json strings and returns true if they have same content/values.`
```scala
    case class Person(id: Long, firstName: String, lastName: String, email: String)
    val payload0: Person = Person(7, "Venkata", "Chekuri", "bharanikrishna7@gmail.com")
    val payload1: Person = Person(9, "Anakin", "Skywalker", "darth.vadar@deathstar.com")
    val json0: String = json.toString[Person](payload0)
    val json1: String = json.toString[Person](payload1)
    println(s"Comparison Result ${json.compare(json0, json1)}")             // this should return false.
    println(s"Comparison Result ${json.compare(json0, json0)}")             // this should return true.
```
##### Note
`Please ensure that the type block is included while making the call to json methods. Without the type block the methods will not be able to find manifests for complex type classes.`

`Example shown below:`
```scala
    case class Numbers(integer: Int, long: Long)
    val json: String = "{\"integer\":1,\"long\":2}"
    try {
    val parsed0: T = json.parse(json)                   // this will not work always since, parse will not be able to find manifest for non-standard objects.
    } catch {
        case e: Exception => {
            println(s"Error while converting Json String to Object : ${e.getMessage}")
        }
    }
    val parsed1: T = json.parse[Numbers](json)          // this will work since, the manifest is retrieved always from the supplied case class in type block.
    
    // similarly for toString
    try {
    val json0: String = json.toString(parsed1)          // this will not work always since, parse will not be able to find manifest for non-standard objects.
    } catch {
        case e: Exception => {
            println(s"Error while converting Object to Json String : ${e.getMessage}")
        }
    }
    val json1: String = json.toString[Numbers](parsed1) // this will work since, the manifest is retrieved alwaus from the supplied case class in type block.
```

### Time
time objects contain commonly used functions related to date / time / timestamp.

`getCurrentTimestamp: this method retrieves the current timestamp. It takes a non-mandatory timezone argument, if supplied the retrieved time will be the current time with respect to the supplied timezone.`
```scala
    val timestamp0: String = time.getCurrentTimestamp()          // get current local timestamp
    val tz: TimeZone = TimeZone.getTimeZone("UTC")
    val timestamp1: String = time.getCurrentTimestamp(tz)       // get UTC local timestamp
```

`stopclock: this method to execute the function block which has been passed to it and log the time taken to execute the block. The result of the function block is returned as result.`
```scala
    def task(load: Int = 100): Int = {
        var result : Int = 0
        for(i <- 1 to load) {
          Thread.sleep(100)
          result = result + 1
        }
        result
    }
    
    val load: Int = 1000
    val result: Int = time.stopclock(task(load))
    if(load != result) {
        println("Something went wrong, both values should be same")
    } else {
        println("Result validated successfully.")
    }
```

### Network
network oject comtains commonly used functions related to http client requests.
`generateURI: this method generates url when path and query string parameters are supplied`
```scala
    val path: String = "http://exapmlepath.com"
    val queryStringParams: Map[String, String] = Map("key1" -> "value1", "key2" -> "value2")
    val url: String = network.generateURI(path, queryStringParams)
    val expected: String = "http://exapmlepath.com?key1=value1&key2=value2"
    if(url != expected) {
        println("Something went wrong, both values should be same")
    } else {
        println("Result validated successfully.")
    }
```

`get: this method will make http get request and retrieve the response. Response body is string.`
```scala
    val path: String = "http://google.com"
    val response: network.Response[String] = network.get(path)
    if(response.status / 10 == 20) {
        println("Result validated successfully.")
    } else {
        println("Something went wrong, the response status code should be 20X. Where X is between 0 - 9")
    }
```

`getPOJO: this method will make http get request and retrieve the response. Response body should have same format as the Type of POJO.`
```scala  
  case class Employee(
      id: String,
      employee_name: String,
      employee_salary: String,
      employee_age: String,
      profile_image: String
  )

  val path: String = "http://dummy.restapiexample.com/api/v1/employee/42705"
  val response: network.Response[Employee] = network.getPOJO[Employee](path)
  if (response.status / 10 == 20) {
    println("Successfully retrieved the object.")
    println(s"Object is : ${json.toString[Employee](response.body)}")
  } else {
    println("Something went wrong, the response status code should be 20X. Where X is between 0 - 9")
  }
```

`getPOJOWithParams: this method will take in query string params and make http get request - retrieving response. The retrieved response will have same format as the Type of POJO.`
```scala
    // usage will be same as above, it'll just take in an extra argument for Query String Params.
    // Unfortuately I don't have an easy way for users to test it.
    // But it works. 
```

`post: this method will make a http post request and retrieve the response. Optional body argument can be supplied, since usually post requests store data on servers.`
```scala
  case class Employee(
      name: String,
      salary: String,
      age: String
  )

  val url: String = "http://dummy.restapiexample.com/api/v1/create"
  val payload: Employee = Employee(name = "test", salary = "123", age = "23")
  val payloadToString: String = json.toString[Employee](payload)

  val response: Response[String] = network.post(url, Some(payloadToString))
  if (response.status / 10 == 20) {
    println("Successfully retrieved the object.")
    println(s"Received Response : ${response.body}")
  } else {
    println("Something went wrong, the response status code should be 20X. Where X is between 0 - 9")
  }
```

`postPOJO: this method will make a http post request with supplied POJO as the body of the request and retrieve response. The retrieved response body is of type string.`
```scala
  case class Employee(
      name: String,
      salary: String,
      age: String
  )

  val url: String = "http://dummy.restapiexample.com/api/v1/create"
  val payload: Employee = Employee(name = "test", salary = "123", age = "23")

  val response: Response[String] = network.postPOJO[Employee](url, payload)
  if (response.status / 10 == 20) {
    println("Successfully retrieved the object.")
    println(s"Received Response : ${response.body}")
  } else {
    println("Something went wrong, the response status code should be 20X. Where X is between 0 - 9")
  }
```
## Benchmarks
* [ ] json
    * [ ] parse
    * [ ] toString
* [ ] network
    * [ ] get
        * [ ] without auth
        * [ ] with auth
        * [ ] with result parsed to POJO/case class
    * [ ] post
        * [ ] without auth
        * [ ] with auth
        * [ ] with POJO/case class in body
